import { Injectable } from '@angular/core';
// import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Materia } from '../interfaces/materia';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class MateriaService {
  private todosCollection: AngularFirestoreCollection<Materia>;
  private todos: Observable<Materia[]>;
  constructor(private db: AngularFirestore) {
    const email = localStorage.getItem('email');
    this.todosCollection = db.collection<Materia>('materia_usuario', ref => ref.where('email_id', '==', email));
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
    // this.todosCollection = this.afDatabase.list('/materia_usuario');
    // ref => ref.equalTo('email_id').equalTo(email)
  }
  getMaterias() {
    return this.todos;
  }
}

import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;
// import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Alumno } from '../interfaces/alumno';
import { Asistencia } from '../interfaces/asistencia';

@Injectable()
export class AsistenciaService {
  private todosCollection: AngularFirestoreCollection<Asistencia>;
  private todos: Observable<Asistencia[]>;

  constructor(private db: AngularFirestore) {
    this.todosCollection = db.collection<Asistencia>('asistencias');
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
  addAsistencia(todo: Asistencia) {
    return this.todosCollection.add(todo);
  }
}

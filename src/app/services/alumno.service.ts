import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;
// import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Alumno } from '../interfaces/alumno';

@Injectable()
export class AlumnoService {
  private todosCollection: AngularFirestoreCollection<Alumno>;
  private todos: Observable<Alumno[]>;

  constructor(private db: AngularFirestore) {
    this.todosCollection = db.collection<Alumno>('alumnos');
    this.todos = this.todosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }
  getTodos() {
    return this.todos;
  }
}

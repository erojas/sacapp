import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, PopoverController, AlertController, ModalController, ToastController } from '@ionic/angular';
import { AlumnoService } from '../../services/alumno.service';
import { Alumno } from '../../interfaces/alumno';
import { Asistencia } from '../../interfaces/asistencia';
import { AsistenciaService } from '../../services/asistencia.service';

@Component({
  selector: 'app-assistance',
  templateUrl: './assistance.page.html',
  styleUrls: ['./assistance.page.scss'],
})
export class AssistancePage implements OnInit {
  alumnos: Alumno[];
  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/img/ionic4-Start-Theme-cover.jpg';

  constructor(public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private alumnoService: AlumnoService,
    private asistenciaService: AsistenciaService
  ) {
    this.alumnoService.getTodos().subscribe(res => {
      this.alumnos = res;
    });
  }

  ngOnInit() {
  }
  goSaveAsistencia(alumno, tipo) {
    const asistencia: Asistencia = {
      alumno_id: alumno.id,
      created_at: new Date().getTime(),
      code: tipo,
      fecha: new Date().toString()
    };
    this.asistenciaService.addAsistencia(asistencia);
  }
  async alertLocation() {
    const changeLocation = await this.alertCtrl.create({
      header: 'Change Location',
      message: 'Type your Address.',
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: async (data) => {
            console.log('Change clicked', data);
            this.yourLocation = data.location;
            const toast = await this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });

            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

}

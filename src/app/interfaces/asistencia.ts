export interface Asistencia {
  alumno_id: number;
  fecha: string;
  code: string;
  created_at: number;
}

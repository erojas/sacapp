export interface Alumno {
  id: number;
  nombre: string;
  apellido_materno: any;
  apellido_paterno: string;
  rude: string;
  ci: string;
}
